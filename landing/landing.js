
    //Append item
    var id = "flex-grid"
    function newElement(title,text){
        return `<div class="flexgrid-item">
            <div class="item-container">
                <div class="inside-container">
                    <h2>${title}</h2>
                    <p>${text}</p>
                </div>
            </div>
        </div>`
    }

    function setId(newId){
        id = newId;
    }

    function insertElement(title, text){
        var grid = document.getElementById(id);
        grid.insertAdjacentHTML('afterbegin',newElement(title,text));
    }

    //Screen control functions

    function setFullWidths() {
        var fw = jQuery(".fullwidth");
        var w = jQuery(window);
        fw.width(w.width());
        fw.offset({
            left: 0
        });
    }

    function setFullHeights() {
        var fw = jQuery(".fullheigth");
        var w = jQuery(window);
        fw.height(w.height());
        fw.offset({
            left: 0
        });
    }

    function setFullScreens(){
        var fw = jQuery(".fullscreen");
        var w = jQuery(window);
        fw.height(w.height());
        fw.width(w.width());
        fw.offset({
            left: 0,
        });
    }
    setFullWidths();
    setFullHeights();
    setFullScreens();
    jQuery(window).resize(setFullWidths);
    jQuery(window).resize(setFullHeights);
    jQuery(window).resize(setFullScreens);

    jQuery(window).scroll(function(){
        var scroll = jQuery(window).scrollTop();
        $(".section-heading").css({
            "background-size":(100 + scroll*0.05)+"%"
        });
    })
