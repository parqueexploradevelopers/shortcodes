
    //Append item
    var id = "flex-grid"
    function newElement(title,text){
        return `<div class="flexgrid-item">
            <div class="item-container">
                <div class="inside-container">
                    <h2>${title}</h2>
                    <p>${text}</p>
                </div>
            </div>
        </div>`
    }

    function newElementWithClass(title,text,selector){
        return `<div class="flexgrid-item">
            <div class="${selector}">
                <div class="inside-container">
                    <h2>${title}</h2>
                    <p>${text}</p>
                </div>
            </div>
        </div>`
    }

    function insertElement(title, text){
        var grid = document.getElementById(id);
        grid.insertAdjacentHTML('afterbegin',newElement(title,text));
    }

   // var e = newElement("Tilo1","texto");
   // insertElement("Titulo1","Descripcion");

    /*
    var flexgrid = document.getElementsByClassName("flexgrid-container")[0];
    var categories = "La Tierra nuestra casa Planetaria,Paara niños de 4 a 7 años;TituloA,A;TituloA,A;TituloA,A".split(";");
    var body = categories.map(element =>{
        var split = element.split(",");
        var tittle = split[0];
        var text = split[1];
        return `<div class="flexgrid-item">
            <div class="item-container">
                <div class="inside-container">
                    <h2>${tittle}</h2>
                    <p>${text}</p>
                </div>
            </div>
        </div>`
    }).join('\n');
    //`<div class="flexgrid-item"><div class="item-container"><div class="inside-container"></div></div></div>`).join('\n');
    console.log(body);
    flexgrid.innerHTML = body;
    */
